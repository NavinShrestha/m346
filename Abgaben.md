# Umsetzung / Abgabe Lernprodukt / Bewertung

[TOC]

# Umsetzung
## Git Repository

Erstellen Sie ein Repository in Gitlab oder Github und berechtigen Sie die Lehrperson(en) mit Leserechten. Sie werden alle Kompetenzen sowie die zugehörenden Lernprodukte in diesem Git Repo abgeben. Planen Sie daher ihre Struktur bereits entsprechend, z. B.

KN01/&lt;Ihre Dateien hier &gt;

KN02/&lt;Ihre Dateien hier &gt;

....

## Vorgehen

Sie werden meistens praktisch in Kompetenzthemen arbeiten. Dass Sie ein Thema verstanden haben, werden Sie beweisen, indem Sie ihre Schritte **und/oder** die Endprodukte mit Screenshots belegen und im Git Repository ablegen. **Verwenden Sie dazu Markdown**! 

**ACHTUNG**: In den meisten Fällen erstellen Sie erst Screenshots, nachdem bei Ihnen alles einwandfrei läuft. Wir empfehlen deshalb, dass Sie während der Umsetzung auch die Schritte dokumentieren, damit Sie die gleichen Schritte nicht mehrfach machen müssen.

Sie dürfen jeweils in 2er Gruppen arbeiten, aber **jeder** Teilnehmer führt **alle** Schritte in seiner eigenen Lernumgebung durch und erstellt sein **eigenes** Repository mit allen Lernprodukten und erforgerlichen Abgaben.

Der Fokus bei der Erfüllung der Kompetenzen steht Ihre persönliche Leistung im Zentrum. Dokumentieren Sie als Ihren Lernfortschritt und insbesondere Ihre Lernerfolge. Das heisst: Die Lehrperson muss anhand von Ihrer Dokumentation nachvollziehen können, ob Sie die Übung funktional erfolgreich umgesetzt haben.
Das erreichen Sie wie folgt:
* Dokumentieren Sie Ihre Lernumgebung mit einer Visualisierung (logischer Netzwerkplan oder Funktionsplan). Integrieren Sie in diese Dokumentation alle relevanten und individuellen zugehörenden Konfigurationsdefinitionen wie IP-Adressen, Portangaben oder Systmnamen.
Achten Sie darauf, dass diese Informatinen deckungsgeleich mit Ihrer Systemdokumentation ist.
* Definieren Sie konkrete Testfälle. Testen Sie Ihre gebaute Systemumgebung entsprechend und dokumentieren Sie diese Tests, insbesondere die Systemrückmeldungen.
* Lesen Sie wo möglich Aktivitätslogs aus Ihren gebauten Systemen und hinterlegen Sie diese im Repsoitory.
* Ein guter Plattformentwickler verwendet für alle Code-Teile (wie Cloud-Init-Files oder Yaml-Files) immer ein Repository. Damit das der Code auch in der Praxis verwendet und weiterentickelt werden kann, müssen Sie alle Code-Teile immer in separates Files speichern. Diese Files können Sie dann mit Ihrer Dokumentation verlinken.

Gutes Beispiel für eine entsprechende System-Dokumentation, welche diese Anforderungen erfüllt:

{: .note} Link auf sep. File, coming soon

## Interpretation der Fragestellung

Wenn in Kompetenzen "Zeigen Sie, dass..." steht, ist damit gemeint, dass Sie die Schritte dokumentieren sollen (in Markdown) und zwar so, dass aus den Screenshots und Text klar wird, welche Aktionen Sie durchgeführt haben.

## Abgabe Lernprodukt
Halten Sie sich an die Vorgaben der Lehrperson.

**Denken Sie bitte mit bei der Abgabe/den Screenshots**. Wenn Sie z.B. einen Screenshot einer Webseite erstellen, aber die URL fehlt, die zeigt, was Sie eigentlich aufrufen, sagt der Screenshot wenig aus. Achten Sie darauf, dass die Screeshots möglichst viel Information von Ihrem individuellem System enthalten, optimalerweise mit Datum und Zeit-Angaben.

## Bewertung
Bewertungsform wird von der Lehrperson kommuniziert. 

[comment]: <ausser wir finden hier einen gemeinsamen nenner, zum Beispile Einsatz von Teams-Bewertungsraster>
