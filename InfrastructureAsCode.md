# Infrastructure As Code

[TOC]

Infrastruktur als Code ist ein Paradigma (grundsätzliche Denkweise) zur Infrastruktur-Automation.

Es basiert auf konsistenten und wiederholbaren Definitionen (Code) für die Bereitstellung von Systemen und deren Konfiguration.

Produkte sind u.a. Cloud-init, Vagrant, TerraForm, CLIs etc. 

#### Definition von Infrastruktur als Code

- Infrastruktur als Code ist ein Ansatz zur Automatisierung der Infrastruktur, der auf Praktiken aus der Softwareentwicklung basiert. 
- Dabei werden konsistente, wiederholbare Prozesse für die Bereitstellung und Änderung von Systemen und deren Konfiguration verwendet.
- Änderungen werden an Deklarationen (Code) vorgenommen und dann durch automatisierte Prozesse, auf Systeme übertragen.
- Infrastruktur als Code hat sich in den anspruchsvollsten Umgebungen bewährt. Für Unternehmen wie Amazon, Netflix, Google und Facebook sind IT-Systeme nicht nur geschäftskritisch. Sie sind das Geschäft!

#### Ziele von Infrastruktur als Code

- Die IT-Infrastruktur unterstützt und ermöglicht Veränderungen, anstatt ein Hindernis oder eine Einschränkung zu sein.
- Änderungen am System sind Routine, ohne Drama oder Stress für Benutzer oder IT-Mitarbeiter.
- IT-Mitarbeiter verbringen ihre Zeit mit wertvollen Dingen, die ihre Fähigkeiten einbeziehen, und nicht mit sich wiederholenden Routineaufgaben.
- Benutzer können die benötigten Ressourcen definieren, bereitstellen und verwalten, ohne dass IT-Mitarbeiter dies für sie tun müssen.
- Teams können sich einfach und schnell von Fehlern erholen, anstatt davon auszugehen, dass Fehler vollständig verhindert werden können.
- Verbesserungen werden kontinuierlich vorgenommen und nicht durch teure und riskante „Urknall“ -Projekte.
- Lösungen für Probleme werden durch Implementierung, Test und Messung bewiesen, anstatt sie in Besprechungen und Dokumenten zu diskutieren.

### YAML

YAML ist eine einfache Textdatei mit einer definierten Syntax, die wir folgend verwenden werden. Es ist einfach zum Lesen. 

<https://yaml.org/>: YAMLs offizielle Seite. Hier finden Sie auch den Link auf die Spezifikation.

Das folgende Beispiel von der YAML Webseite zeigt die wichtigsten Elemente. Sie haben grundsätzlich die Elemente *Collections* und *Scalars*, die unterschiedlich gemappt werden (z.B. key-value-pairs oder Aufzählungen). 

![yaml](./x_gitres/yaml.png)

### Cloud-init

Cloud-init bietet eine Konfiguration des Betriebssystem während der Installation. Stellen Sie sich folgendes Szenario vor. Sie müssen 10 Server installieren mit verschiedenen Betriebssystemen (Ubuntu, Redhat). Alle benötigen die gleiche Konfiguration betreffend Software/Paketen.  Sie haben die folgenden Optionen:

- Sie installieren alle Server einzeln und installieren alle Pakete und führen die verschiedenen Befehle aus.
- Sie erstellen ein Bash-Script, welches alle Befehle ausführt. Sie müssen es nur noch ausführen. Trotzdem haben Sie noch den Aufwand das Skript auf alle Server zu laden und auszuführen.
- Sie erstellen eine Konfiguration-Datei und geben diese bei der Installation der Instanz mit. Keine weiteren Schritte sind notwendig. Dies ist genau das, was Cloud-init ihnen bietet.

Es wird hier klar, welche Variante bevorzugt wird. Die Automatisierung kann natürlich noch weiter getrieben werden, aber dies kommt in einem späteren Kapitel.

Die Referenz zu Cloud-init Dateien ist umfangreich. Sie werden in der Kompetenz KN02 die wichtigsten Elemente und Konfigurationen kennenlernen und anwenden.

<https://cloudinit.readthedocs.io/en/latest/>: Cloud-inits offzielle Seite. 



### Command Line Interfaces

Mit Cloud-Init haben Sie die Installation eines Servers automatisiert. Die Command Line Interfaces (CLIs) der verschiedenen Anbieter bieten Unterstützung bei der **Automatisierung der Infrastruktur**. Sie können mit diesen CLIs Instanzen, Sicherheitsgruppen, Netzwerk-Interfaces, etc erstellen. Dies ist also eine zusätzliche Ebene der Automatisierung. Natürlich können Cloud-Init Konfigurationen auch gleich mit geladen werden, so dass die erstellten Objekte auch korrekt Initialisiert werden.

Beispiel zu CLIs der Anbieter:

- AWS: <https://aws.amazon.com/cli/>
- Azure: <https://learn.microsoft.com/en-us/cli/azure/install-azure-cli>
- Google: <https://cloud.google.com/sdk/gcloud>
- Heroku: https://devcenter.heroku.com/articles/heroku-cli



### TerraForm

TerraForm ist ein Anbieter, der Cloud-Provider unabhängig ist. Mit Terraform können sie also die Infrastruktur aller Anbieter verwalten. Die Sprache ist soweit entwickelt, dass Sie direkt auch mit Rückgabewerten arbeiten können. Stellen Sie sich vor, Sie erstellen eine IP-Adresse. Die neu erstellte IP-Adresse hat eine ID (oder Referenz), die Sie vorher nicht kannten. Wenn Sie mit CLIs arbeiten, müssen Sie diese manuell oder mit einem eigenen Skript aus dem Rückgabewert des CLI-Requests auslesen und im nächsten Befehl einfügen bevor Sie den nächsten Befehl ausführen können.

TerraForm bietet die Möglichkeit, die IDs direkt auszulesen und als Variable in der nächsten Ressource direkt zu verwenden. Auf diese Weise können Sie die gesamte Infrastruktur in einem Skript und einem Befehl erstellen und dies sogar über mehrere Cloud-Provider hinweg.

Natürlich benötigen Sie auch für TerraForm eine CLI: <https://developer.hashicorp.com/terraform>.



### Quellen

Infrastructure As Code: [Cloud Native Kurs TBZ/HF](https://gitlab.com/ch-tbz-hf/Stud/cnt/-/tree/main/2_Unterrichtsressourcen/B)

Screenshot YAML: [Offizielle Webseite](https://yaml.org/spec/1.2.2/#21-collections)





