# Backup / Cleanup Script



In diesem Ordnern finden Sie zwei Python-Skripts, die sie für ein Backup und späteres Aufräumen Ihrer Instanzen verwenden können. Die beiden Skripts wurden verändert für Python 3.9 (Quelle: medium.com).

[Backup-Skript](./backup.py)

[Cleanup-Skript](./cleanup.py)

**Funktionsweise**

Sie müssen Ihrer Instanz den Tag "Backup" hinzufügen. Das **Cleanup-**Skript sucht alle Instanzen mit diesem Tag und erstellt einen Snapshot, ebenfalls mit Tag "DeleteOn" und einem Daten 7 Tage in der Zukunft.

![tag](./tag.png)

Das **Cleanup-**Skript sucht nach allen Snapshots mit dem "DeleteOn"-Tag und löscht diese, falls das Datum überschritten wird.

![snapshots](./snapshots.png)



## Quellen

medium.com: [Automating Backups using  AWS Lambda](https://medium.com/cognitoiq/automating-backups-using-aws-lambda-baa013fdffc7)

